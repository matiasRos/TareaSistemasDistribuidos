package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignaturas  implements Serializable {

	
	Long idasig;
	String nomasig;
	
	List <Persona> Alumnos;
	
	public Asignaturas() {
		Alumnos = new ArrayList<Persona>();
	}
	
	public Asignaturas(Long id, String nombre) {
		this.idasig = id;
		this.nomasig = nombre;
		
		Alumnos = new ArrayList<Persona>();
	}
	
	
	public Long getId() {
		return this.idasig ;
	}
	
	public void setID(Long ID) {
		this.idasig = ID;
	}
	
	public String getNombre() {
		return this.nomasig;
	}
	
	public void setNombre(String nombre) {
		this.nomasig = nombre;
	}
	
	public List<Persona> getAlumnos(){
		return this.Alumnos;
	}
	
	public void setAumnos(List<Persona> Alumnos) {
		this.Alumnos = Alumnos;
	}
}
